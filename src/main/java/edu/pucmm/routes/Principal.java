package edu.pucmm.routes;

import spark.Request;
import spark.Response;
import spark.Route;

import java.util.concurrent.TimeUnit;

public class Principal implements Route {

    @Override
    public Object handle(Request request, Response response) throws Exception {
        System.out.println("Escribo un texto justo ahora para saber que se ejecuta despues");
        TimeUnit.SECONDS.sleep(5); // Mas lag para que en la consola se vea el caso.
        return "Hola mundo!";
    }
}

package edu.pucmm.filters;

import spark.Filter;
import spark.Request;
import spark.Response;

public class DemoAfterFilter implements Filter {

    @Override
    public void handle(Request request, Response response) throws Exception {
        System.out.println("Me ejecuto despues del URL");
    }
}

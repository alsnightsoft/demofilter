package edu.pucmm.filters;

import spark.Filter;
import spark.Request;
import spark.Response;

import java.util.concurrent.TimeUnit;

public class DemoBeforeFilter implements Filter {

    @Override
    public void handle(Request request, Response response) throws Exception {
        System.out.println("Me ejecuto antes de cada URL visitada");
        TimeUnit.SECONDS.sleep(10); // espero 10 segundos para provocar un lag y puedan ver que es asi, pero no es necesario
    }
}

package edu.pucmm;

import edu.pucmm.filters.DemoAfterFilter;
import edu.pucmm.filters.DemoBeforeFilter;
import edu.pucmm.routes.Principal;
import spark.Spark;

public class SparkDemo {

    public static void main(String[] args) {
        Spark.port(8070);
        Spark.after(new DemoAfterFilter());
        Spark.before(new DemoBeforeFilter());
        // Cualquier caso se puede aplicar el path a donde quieren filtrar.
        Spark.get("/spark", new Principal());
        Spark.init();
    }
}

package edu.pucmm.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParamController {

    @RequestMapping("/spring")
    public String index() {
        return "Hello World!";
    }

}
